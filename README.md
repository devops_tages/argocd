# ArgoCD

```bash
./pull.sh
```

```bash
kubectl create namespace argocd
```

```bash
kubectl apply -f bundle/argo-cd --recursive
```

---

```bash
git add .
```

```bash
git commit -m "argo 5.53.7"
```

```bash
git push
```

---

```bash
kubectl -n argocd get secret argocd-initial-admin-secret -o jsonpath="{.data.password}" | base64 -d
```

```bash
kubectl port-forward --address 0.0.0.0 service/argo-argocd-server -n argocd 8080:443
```

---

```bash
kubectl apply -f bundle/argoproject-system/ --recursive
kubectl apply -f bundle/argoproject-development/ --recursive
```

```bash
kubectl rollout restart deploy argo-argocd-server -n argocd
```

---

```bash
./pull.sh
```

---

```bash
git add .
```

```bash
git commit -m "argo 5.53.14"
```

```bash
git push
```

```
test
```