#!/bin/bash
rm -rf cache
helm pull argo-cd --untar --destination cache/. --version 5.53.7 --repo "https://argoproj.github.io/argo-helm"
rm -rf bundle/argo-cd
helm template argo cache/argo-cd -f cache/argo-cd/values.yaml -f values.yaml --output-dir bundle/. --namespace argocd
#test git